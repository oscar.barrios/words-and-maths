﻿using UnityEngine;
using System.Collections;
using System;

public class ShapeMovementController : MonoBehaviour {

    public Transform rotationPivot;
    public float transitionInterval = 0.8f;
    public float fastTransitionInterval ;
    float lastFall;
    bool win;
    bool answered;
    int mercy;

    public void ShapeUpdate()
    {
        if(answered){
            InstantFall();
        }else{
            FreeFall();
        }
    }

    public void RotateClockWise(bool isCw)
    {
        float rotationDegree = (isCw) ? 90.0f : -90.0f;

        transform.RotateAround(rotationPivot.position, Vector3.forward, rotationDegree);

        // Check if it's valid          
        if (Managers.Grid.IsValidGridPosition(this.transform)) // It's valid. Update grid.
        {
            Managers.Grid.UpdateGrid(this.transform);
        }
        else // It's not valid. revert rotation operation.
        {
            transform.RotateAround(rotationPivot.position, Vector3.forward, -rotationDegree);
        }
    }


    public void Move(Vector3 movement)
    {
        // Modify position
        transform.position += movement;

        // Check if it's valid
        if (Managers.Grid.IsValidGridPosition(this.transform))// It's valid. Update grid.
        {
            Managers.Grid.UpdateGrid(this.transform);
        }
        else // It's not valid. revert movement operation.
        {
            transform.position -= movement;
        }
    }

    public void MoveHorizontal(Vector2 direction)
    {
        float deltaMovement = (direction.Equals(Vector2.right)) ? 1.0f : -1.0f;

        // Modify position
        transform.position += new Vector3(deltaMovement, 0, 0);

        // Check if it's valid
        if (Managers.Grid.IsValidGridPosition(this.transform)) // It's valid. Update grid.
        {
            Managers.Grid.UpdateGrid(this.transform);
        }
        else // It's not valid. revert movement operation.
        {
            transform.position += new Vector3(-deltaMovement, 0, 0);
        }
    }

    public void FreeFall()
    {
        float waitingTime = Math.Max(0.2f, (transitionInterval + Managers.Game.temporalSpeedBonus - (float)(Managers.Score.currentScore - Managers.Score.extraLifes) * 0.004f));
        if (Time.time - lastFall >= waitingTime)
        {
            if (mercy++ < 2.0f / waitingTime) {
                lastFall = Time.time;
                return; 
            }

            Fall();
        }
    }

    public void InstantFall()
    {
        if (Time.time - lastFall >= fastTransitionInterval)
        {
            Fall();
        }
    }

    public void Fall(){

        // Modify position
        transform.position += Vector3.down;

        // See if valid
        if (Managers.Grid.IsValidGridPosition(this.transform))
        {
            // It's valid. Update grid.
            Managers.Grid.UpdateGrid(this.transform);
            if (!win) Managers.Audio.PlayDropSound();
        }
        else
        {
            if (!win)
            {
                // Word raised red line!
                Managers.Score.OnLostWord();
                Managers.UI.OnWordLost();
                if (Managers.Audio.soundSource.clip != Managers.Audio.loseSound)
                { 
                    Managers.Audio.PlayLoseWordSound();
                }
            }
            else
            {
                Managers.UI.OnWordCompleted();
                Managers.Audio.PlayWinWordSound();
                Managers.Score.OnScore(Managers.Game.currentShape.type);
            }

            win = false;
            mercy = 0;
            GetComponent<ShapeMovementController>().enabled = false;
            GetComponent<WordShape>().enabled = false;
            Managers.Game.currentShape.gameObject.SetActive(false);
            Managers.Game.currentShape = null;

            // Clear filled horizontal lines
            Managers.Grid.PlaceShape();
        }

        lastFall = Time.time;
    }


    internal void Win(){
        win = true;
        answered = true;
        if (Managers.Game.temporalSpeedBonus > 0){
            Managers.Game.temporalSpeedBonus -= 0.05f;
        }
    }

    internal void Lose()
    {
        answered = true;
    }
}
