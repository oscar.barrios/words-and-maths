﻿using UnityEngine;
using System.Linq;
using System;

public enum ShapeType
{
    Three = 0,
    Four = 1,
    Five = 2,
    Six = 3,
    Seven = 4,
    Eight = 5,
    Nine = 6,
    Ten = 7,
    Maths = 8
}

public enum GameMode
{
    CountLetters = 0,
    CountVowels = 1,
    CountConsonants = 2,
    ReverseWord = 3,
    Maths = 4
}

public class WordShape : MonoBehaviour
{
    [HideInInspector]
    public ShapeType type;

    [HideInInspector]
    public ShapeMovementController movementController;

    [HideInInspector]
    public string word;

    public static int minLenght = 3;
    public static int maxLenght = 10;

    void Awake()
    {
        movementController = GetComponent<ShapeMovementController>();
    }

    void Start()
    {
        // Default position not valid? Then it's game over
        if (!Managers.Grid.IsValidGridPosition(this.transform))
        {
            Managers.Game.SetState(typeof(GameOverState));
            Destroy(this.gameObject);
        }

        var textMeshes = this.gameObject.GetComponentsInChildren<TextMesh>();
        foreach (int i in Enumerable.Range(0, textMeshes.Length)){
            if (i < word.Length)
            {
                textMeshes[i].text = word[i].ToString();
            }else{
                textMeshes[i].text = "";
                textMeshes[i].transform.parent.gameObject.SetActive(false);
            }
        }

        transform.position += new Vector3(0, 4, 0);
        int randomMove = UnityEngine.Random.Range(-4, 4);
        movementController.Move(new Vector3(randomMove, 0, 0));
    }

    public void AssignColor(int index)
    {
        Color temp = Managers.Palette.themePack_Gram[index];
        foreach (SpriteRenderer renderer in GetComponentsInChildren<SpriteRenderer>().ToList())
        {
            renderer.color = temp;
        }
    }

}
