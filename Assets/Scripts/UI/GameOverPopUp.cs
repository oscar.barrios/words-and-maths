﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class GameOverPopUp : MonoBehaviour {

    public Text gameOverLabel;
    public Text gameOverScore;

    void Awake()
    {
        gameOverLabel.text = Managers.Localization.Localize("GameOver.Info");
    }

    void OnEnable()
    {
        gameOverScore.text = Managers.Score.currentScore.ToString();
        Managers.UI.panel.SetActive(true);
    }

    public void BackToMainMenu()
    {
        Managers.Grid.ClearBoard();
        Managers.Score.ResetScore();
        Managers.Audio.PlayUIClick();
        Managers.UI.panel.SetActive(false);
        Managers.Game.SetState(typeof(MenuState));
        gameObject.SetActive(false);
    }
}
