﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BonusAdsButton : MonoBehaviour {

    public Text info;

    public void OnClickBonusAdsButton()
    {
        Managers.Audio.PlayUIClick();
        Managers.Game.SetState(typeof(MenuState));
        Managers.UI.panel.SetActive(true);
        Managers.UI.popUps.ActivateBonusAdsPopUp();
        Managers.Anal.SendBonusAdsClickAnalytic();
    }

    public void Start()
    {
        info.text = Managers.Localization.Localize("BonusAds.Info");
    }
}
