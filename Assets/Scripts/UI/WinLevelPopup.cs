﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class WinLevelPopup : MonoBehaviour {
    
    public Text level;
    public Text levelCompletedLabel;

    void OnEnable()
    {
        level.text = Managers.Game.stats.level.ToString();
        levelCompletedLabel.text = Managers.Localization.Localize("WinLevel.Info");
        Managers.UI.panel.SetActive(true);
    }

    public void BackToMainMenu()
    {
        Managers.Grid.ClearBoard();
        Managers.Score.ResetScore();
        Managers.Audio.PlayUIClick();
        Managers.UI.panel.SetActive(false);
        Managers.Game.SetState(typeof(MenuState));
        gameObject.SetActive(false);
    }
}
