﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public GameObject soundCross;

    public void TurnUpDownSound()
    {
        if (AudioListener.volume == 0)
        {
            soundCross.SetActive(false);
            AudioListener.volume = 1.0f;
            Managers.Audio.PlayUIClick();
        }
        else if (AudioListener.volume == 1.0f)
        {
            soundCross.SetActive(true);
            AudioListener.volume = 0f;
        }
    }

    public void OpenFacebookPage()
    {
        Application.OpenURL(Constants.FACEBOOK_URL);
    }

    public void OpenContact()
    {
        Application.OpenURL(Constants.CONTACT_URL);
    }

    public void RateAsset()
    {
#if UNITY_IOS
        Application.OpenURL(Constants.APPLE_STORE_URL);
#else
        Application.OpenURL(Constants.GOOGLE_STORE_URL);
#endif
    }
}
