﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class MainMenu : MonoBehaviour
{
    
    public GameObject title;
    public GameObject menuButtons;
    public GameObject restartButton;

    public GameObject tooltipPlay;
    public Text toottipPlayText;

    public HorizontalLayoutGroup layout;

    void Awake()
    {
        layout = GetComponent<HorizontalLayoutGroup>();
    }

    void OnEnable()
	{
        menuButtons.SetActive (true);
        title.SetActive(true);
    }

	void OnDisable()
	{
        title.SetActive(false);
        tooltipPlay.SetActive(false);
		menuButtons.SetActive (false); 
    }

    public void DisableMenuButtons ()
	{
        title.SetActive(false);
		menuButtons.SetActive (false);
	}

    public void MainMenuStartAnimation()
    {
        menuButtons.GetComponent<RectTransform>().DOAnchorPosY(-650, 1, true);
    }

    public void MainMenuEndAnimation()
    {
        menuButtons.GetComponent<RectTransform>().DOAnchorPosY(-1050, 0.3f, true);
    }

}

