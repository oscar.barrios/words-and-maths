﻿using UnityEngine;
using System.Collections;
using System;

public class PopUp : MonoBehaviour
{
    public GameObject gameOverPopUp;
    public GameObject settingsPopUp;
    public GameObject playerStatsPopUp;
    public GameObject bonusAdsPopUp;
    public GameObject winLevelPopup;

    public void ActivateGameOverPopUp()
    {
        gameOverPopUp.transform.parent.gameObject.SetActive(true);
        gameOverPopUp.SetActive(true);
        Managers.UI.activePopUp = gameOverPopUp;
    }

    internal void ActivateGameWinLevelPopup()
    {
        winLevelPopup.transform.parent.gameObject.SetActive(true);
        winLevelPopup.SetActive(true);
        Managers.UI.activePopUp = winLevelPopup;
    }

    internal void ActivateBonusAdsPopUp()
    {
        bonusAdsPopUp.transform.parent.gameObject.SetActive(true);
        bonusAdsPopUp.SetActive(true);
        Managers.UI.activePopUp = bonusAdsPopUp;
    }

    public void ActivateSettingsPopUp()
    {
        settingsPopUp.transform.parent.gameObject.SetActive(true);
        settingsPopUp.SetActive(true);
        Managers.UI.activePopUp = settingsPopUp;
    }

    public void ActivatePlayerStatsPopUp()
    {
        playerStatsPopUp.transform.parent.gameObject.SetActive(true);
        playerStatsPopUp.SetActive(true);
        Managers.UI.activePopUp = playerStatsPopUp;
    }

}
