﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatsUI : MonoBehaviour {

    public Text levelLabel;
    public Text highScoreLabel;
    public Text totalScoreLabel;
    public Text timeSpentLabel;
    public Text numberOfGamesLabel;
    public Text title;

    public Text level;
    public Text highScore;
    public Text totalScore;
    public Text timeSpent;
    public Text numberOfGames;

    public void ClearStats()
    {
        Managers.Game.stats.ClearStats();
        RefreshText();
    }

    void Awake()
    {
        title.text = Managers.Localization.Localize("Stats.MyProfile");
        levelLabel.text = Managers.Localization.Localize("Stats.Level");
        highScoreLabel.text = Managers.Localization.Localize("Stats.HighScore");
        totalScoreLabel.text = Managers.Localization.Localize("Stats.TotalScore");
        timeSpentLabel.text = Managers.Localization.Localize("Stats.TimeSpent");
        numberOfGamesLabel.text = Managers.Localization.Localize("Stats.NumberOfGames");
    }

    void OnEnable()
    {
        RefreshText();
    }

    void RefreshText()
    {
        level.text = Managers.Game.stats.level.ToString();
        highScore.text = Managers.Game.stats.highScore.ToString();
        totalScore.text = Managers.Game.stats.totalScore.ToString();
        timeSpent.text = TimeUtil.SecondsToHMS(Managers.Game.stats.timeSpent);
        numberOfGames.text = Managers.Game.stats.numberOfGames.ToString();
    }

}
