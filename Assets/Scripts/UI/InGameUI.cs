﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class InGameUI : MonoBehaviour {

	public Text score;
    public Text tooltipScoreText;

    public Text highScore;
    public Text tooltipHighScoreText;

    public Text extraLifes;
    public Text tooltipExtraLifesText;

    public Image scoreLabel;
    public Image highScoreLabel;
    public Image lostWordsLabel;
    public Text congratsLabel;

    
    public GameObject gameOverPopUp;

    public int posY = -50;

    public void UpdateScoreUI()
	{
        extraLifes.text = Managers.Score.extraLifes.ToString();
        score.text = Managers.Score.currentScore.ToString();
        highScore.text = Managers.Score.highScore.ToString();
    }

    public void InGameUIStartAnimation()
    {
        scoreLabel.rectTransform.DOAnchorPosY(posY * 3, 1, true);
        score.rectTransform.DOAnchorPosY(posY * 4.2f, 1, true);

        lostWordsLabel.rectTransform.DOAnchorPosY(posY * 8, 1, true);
        extraLifes.rectTransform.DOAnchorPosY(posY * 9.5f, 1, true);

        highScoreLabel.rectTransform.DOAnchorPosY(posY * 13, 1, true);
        highScore.rectTransform.DOAnchorPosY(posY * 14.4f, 1, true);
    }

    public void InGameUIEndAnimation()
    {
        scoreLabel.rectTransform.DOAnchorPosY((posY * 3) + 650, 0.3f, true);
        score.rectTransform.DOAnchorPosY(posY * 4.2f + 650, 0.3f, true);

        lostWordsLabel.rectTransform.DOAnchorPosY((posY * 8) + 650, 0.3f, true);
        extraLifes.rectTransform.DOAnchorPosY((posY * 9.5f) + 650, 0.3f, true);

        highScoreLabel.rectTransform.DOAnchorPosY((posY * 13) + 650, 0.3f, true);
        highScore.rectTransform.DOAnchorPosY((posY * 14.4f) + 650, 0.3f, true);
    }


}
