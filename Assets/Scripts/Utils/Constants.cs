﻿using UnityEngine;
using System.Collections;

public static class Constants
{
    public static string GAME_NAME = "Hurry Mind!";
    public static string APPLE_STORE_URL = "http://itunes.apple.com/app/id1425491845";
    public static string GOOGLE_STORE_URL = "https://play.google.com/store/apps/details?id=com.oscarbarrios.hurrymind";
    public static string FACEBOOK_URL = "https://www.facebook.com/Oubiti-2139217333070566/";
    public static string CONTACT_URL = "https://www.linkedin.com/in/oscarbarrios/";
}
