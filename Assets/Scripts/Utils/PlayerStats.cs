﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class PlayerStats: ScriptableObject
{
    public int highScore = 0;
    public int totalScore = 0;
    public float timeSpent = 0f;
    public int numberOfGames = 0;
    public int level = 1;

    public void ClearStats()
    {
        level = 1;
        highScore = 0;
        totalScore = 0;
        timeSpent = 0f;
        numberOfGames = 0;
    }

    #if UNITY_EDITOR
    [MenuItem("Assets/Create/PlayerStatistics")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<PlayerStats> ();
	}
	#endif

}
