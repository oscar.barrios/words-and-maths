﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class Column
{
    public Transform[] row = new Transform[15];
}

[System.Serializable]
public class Row
{
    public Transform[] col = new Transform[10];
}

public class GridManager : MonoBehaviour
{
    
    int rows = 15;
    int cols = 10;
    int firstRowPosition = 5;
    public GameObject[] flashBlocks;
    public Column[] gameGridcol = new Column[10];
    public Row[] grid = new Row[15];

    public bool InsideBorder(Vector2 pos)
    {
        return ((int)pos.x >= 0 && (int)pos.x < cols && (int)pos.y >= firstRowPosition);
    }

    internal void DrawLevels()
    {
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < cols; x++)
            {
                if (grid[y].col[x] != null)
                {
                    if (Managers.Game.stats.level > x + (y * 10))
                    {
                        grid[y].col[x].GetComponentInChildren<SpriteRenderer>().color = Managers.Palette.themePack_Pastel[1];
                    }
                    else
                    {
                        grid[y].col[x].GetComponentInChildren<SpriteRenderer>().color = Managers.Palette.themePack_Pastel[2];
                    }
                }
            }
        }
    }

    public void PlaceShape()
    {
        int k = 0;

        StartCoroutine(DeleteRows(k));

    }

    IEnumerator DeleteRows(int k)
    {
        for (int y = k; y < rows; ++y)
        {
            if (IsRowFull(y))
            {
                DeleteRow(y);
                DecreaseRowsAbove(y + 1);
                --y;
                Managers.Audio.PlayWinWordSound();
                yield return new WaitForSeconds(0.3f);
            }
        }

        foreach (Transform t in Managers.Game.blockHolder)
        {
            if (t.childCount <= 1)
            {
                Destroy(t.gameObject);
            }
        }

        //New shape will be spawned
        if (Managers.Game.isGameActive)
        {
            Managers.Spawner.Spawn();
        }

        yield break;
    }

    public bool IsRowFull(int y)
    {
        for (int x = 0; x < cols; ++x)
        {
            if (gameGridcol[x].row[y] == null)
                return false;
        }
        return true;
    }

    public void DeleteRow(int y)
    {
        for (int x = 0; x < cols; ++x)
        {
            Destroy(gameGridcol[x].row[y].gameObject);
            gameGridcol[x].row[y] = null;
        }
    }

    public void DecreaseRowsAbove(int y)
    {
        for (int i = y; i < rows; ++i)
        {
            DecreaseRow(i);
        }
    }

    public void DecreaseRow(int y)
    {
        for (int x = 0; x < cols; ++x)
        {
            if (gameGridcol[x].row[y] != null)
            {
                // Move one towards bottom
                gameGridcol[x].row[y - 1] = gameGridcol[x].row[y];
                gameGridcol[x].row[y] = null;

                // Update Block position
                gameGridcol[x].row[y - 1].position += new Vector3(0, -1, 0);
            }
        }
    }

    public bool IsValidGridPosition(Transform obj)
    {
        foreach (Transform child in obj)
        {
            if (child.gameObject.tag.Equals("Block"))
            {
                Vector2 v = Vector2Extension.roundVec2(child.position);

                if (!InsideBorder(v))
                {
                    return false;
                }

                // Block in grid cell (and not part of same group)?
                //if (gameGridcol[(int)v.x].row[(int)v.y] != null &&
                //    gameGridcol[(int)v.x].row[(int)v.y].parent != obj)
                //{
                //    return false;
                //}
            }
        }
        return true;
    }

    public void UpdateGrid(Transform obj)
    {
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < cols; x++)
            {
                if (gameGridcol[x].row[y] != null)
                {
                    if (gameGridcol[x].row[y].parent == obj)
                        gameGridcol[x].row[y] = null;
                }
            }
        }

        foreach (Transform child in obj)
        {
            try
            {
                if (child.gameObject.tag.Equals("Block"))
                {
                    Vector2 v = Vector2Extension.roundVec2(child.position);
                    gameGridcol[(int)v.x].row[(int)v.y] = child;
                }
            }catch(Exception e){
                Debug.LogWarning("Error updating grid: " + e.Message);
            }
        }
    }

    public void GreenFlash(){
        for (int x = 0; x < flashBlocks.Length; x++)
        {
            SpriteRenderer sprite = flashBlocks[x].gameObject.GetComponentInChildren<SpriteRenderer>();
            sprite.color = Managers.Palette.themePack_Pastel[1];
        }
    }

    public void RedFlash()
    {
        for (int x = 0; x < flashBlocks.Length; x++)
        {
            SpriteRenderer sprite =flashBlocks[x].gameObject.GetComponent<SpriteRenderer>();
            sprite.color = Managers.Palette.themePack_Pastel[0];
        }
    }

    public void DisableFlash()
    {
        for (int x = 0; x < flashBlocks.Length; x++)
        {
            SpriteRenderer sprite = flashBlocks[x].gameObject.GetComponent<SpriteRenderer>();
            sprite.color = Managers.Palette.themePack_Pastel[2];
        }
    }

    public void ClearBoard()
    {
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < cols; x++)
            {
                if (gameGridcol[x].row[y] != null)
                {
                    Destroy(gameGridcol[x].row[y].gameObject);
                    gameGridcol[x].row[y] = null;
                }

                if (grid[y].col[x] != null){
                    grid[y].col[x].GetComponentInChildren<SpriteRenderer>().color = Managers.Palette.themePack_Pastel[2];
                }
            }
        }

        foreach (Transform t in Managers.Game.blockHolder)
        {
            Destroy(t.gameObject);
        }
    }


    public void RepaintVoidBoard()
    {
        for (int y = 0; y < rows; y++)
        {
            for (int x = 0; x < cols; x++)
            {
                if (grid[y].col[x] != null)
                {
                    grid[y].col[x].GetComponentInChildren<SpriteRenderer>().color = Managers.Palette.themePack_Pastel[2];
                }
            }
        }

    }

}
