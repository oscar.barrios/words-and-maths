﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

/// <summary>
/// Localization manager.
/// </summary>
public class LocalizationManager : MonoBehaviour
{
	/// <summary>
	/// Fired when localization changed.
	/// </summary>
    public event Action LocalizationChanged = () => { }; 

    readonly Dictionary<string, Dictionary<string, string>> Dictionary = new Dictionary<string, Dictionary<string, string>>();
    string _language = "English";
    string[] _words = { };

    public void Awake()
    {
        Read();
        Language = Application.systemLanguage.ToString();
        Words = LoadWords();
    }

	/// <summary>
	/// Get or set language.
	/// </summary>
    public string Language
    {
        get { return _language; }
        set { _language = value; LocalizationChanged(); }
    }


    /// <summary>
    /// Get or set words.
    /// </summary>
    public string[] Words
    {
        get { return _words; }
        set { _words = value; }
    }


	/// <summary>
	/// Set default language.
	/// </summary>
    public void AutoLanguage()
    {
        Language = "English";
    }

	/// <summary>
	/// Read localization spreadsheets.
	/// </summary>
	public void Read(string path = "Localization")
    {
        if (Dictionary.Count > 0) return;

        var textAssets = Resources.LoadAll<TextAsset>(path);

        foreach (var textAsset in textAssets)
        {
            var text = ReplaceMarkers(textAsset.text);
            var matches = Regex.Matches(text, "\".+?\"");

            foreach (Match match in matches)
            {
                text = text.Replace(match.Value, match.Value.Replace("\"", null).Replace(",", "[comma]"));
            }

            var lines = text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            var languages = lines[0].Trim().Split(',').ToList();

            for (var i = 1; i < languages.Count; i++)
            {
                if (!Dictionary.ContainsKey(languages[i]))
                {
                    Dictionary.Add(languages[i], new Dictionary<string, string>());
                }
            }

            for (var i = 1; i < lines.Length; i++)
            {
                var columns = lines[i].Split(',').Select(j => j.Replace("[comma]", ",")).ToList();
                var key = columns[0];

                for (var j = 1; j < languages.Count; j++)
                {
                    Dictionary[languages[j]].Add(key, columns[j]);
                }
            }
        }

        AutoLanguage();
    }

	/// <summary>
	/// Get localized value by localization key.
	/// </summary>
    public string Localize(string localizationKey)
    {
        if (Dictionary.Count == 0)
        {
            Read();
        }

        if (!Dictionary.ContainsKey(Language)) throw new KeyNotFoundException("Language not found: " + Language);
        if (!Dictionary[Language].ContainsKey(localizationKey)) throw new KeyNotFoundException("Translation not found: " + localizationKey);

        return Dictionary[Language][localizationKey];
    }

    /// <summary>
    /// Get localized value by localization key.
    /// </summary>
    internal string Localize(string localizationKey, params object[] args)
    {
        var pattern = Localize(localizationKey);

        return string.Format(pattern, args);
    }

    private string ReplaceMarkers(string text)
    {
        return text.Replace("[Newline]", "\n");
    }

    /// <summary>
    /// Get localized words
    /// </summary>
    internal string[] LoadWords(string path = "Words")
    {
        TextAsset[] textAssets = Resources.LoadAll<TextAsset>(path + "/" + Language);
        return Regex.Split(textAssets[0].text, "\n|\r|\r\n")
                    .Where( text => text.Length >= WordShape.minLenght && text.Length <= WordShape.maxLenght)
                    .ToArray();
    }
}
