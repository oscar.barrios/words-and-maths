﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class GameManager : MonoBehaviour
{
	public bool isGameActive;
    public WordShape currentShape;
    public Transform blockHolder;
    public PlayerStats stats;
    public Text gameModeLabel;
    public GameMode gameMode;
    public int winOption;
    public float temporalSpeedBonus = 0.0f;

    string filePath;

    IEnumerable<char> vowels = new HashSet<char> { 'a', 'e', 'i', 'o', 'u', 
                                                   'á', 'é', 'í', 'ó', 'ú',
                                                   'à', 'è', 'ì', 'ò', 'ù',
                                                   'ä', 'ë', 'ï', 'ö', 'ü',
                                                   'â', 'ê', 'î', 'ô', 'û'};
    System.Random random = new System.Random();

    void Awake()
	{
		isGameActive = false;
        filePath = Application.persistentDataPath + "/stats.json";
        Managers.Game.LoadPLayerStats();
	}

	private _StatesBase currentState;
    public _StatesBase State
	{
		get { return currentState; }
	}

	//Changes the current game state
	public void SetState(System.Type newStateType)
	{
		if (currentState != null)
		{
			currentState.OnDeactivate();
		}

		currentState = GetComponentInChildren(newStateType) as _StatesBase;
		if (currentState != null)
		{
			currentState.OnActivate();
		}
	}

	void Update()
	{
		if (currentState != null)
		{
			currentState.OnUpdate();
		}
	}

	void Start()
	{
		SetState(typeof(MenuState));
	}

    public void SetUp(string word, GameMode gameMode)
    {
        this.gameMode = gameMode;
        currentShape.AssignColor((int)gameMode);
        currentShape.word = word;
        gameModeLabel.text = Managers.Localization.Localize("GameMode." + gameMode.ToString());
        gameModeLabel.color = Managers.Palette.themePack_Gram[(int)gameMode];
        winOption = random.Next(0, 3);


        switch(gameMode){
            case GameMode.CountLetters:
                Managers.UI.options[0].text = Math.Abs(word.Length + random.Next(1, 5)).ToString();
                Managers.UI.options[1].text = Math.Abs(word.Length + random.Next(-5, -1)).ToString();
                Managers.UI.options[2].text = Math.Abs(word.Length + random.Next(-5, -1)).ToString();
                Managers.UI.options[3].text = Math.Abs(word.Length + random.Next(1, 5)).ToString();
                Managers.UI.options[winOption].text = word.Length.ToString();
                break;

            case GameMode.CountConsonants:
                int countConsonants = word.Count(c => !vowels.Contains(c));
                Managers.UI.options[0].text = Math.Abs(countConsonants + random.Next(-5, -1)).ToString();
                Managers.UI.options[1].text = Math.Abs(countConsonants + random.Next(-5, -1)).ToString();
                Managers.UI.options[2].text = Math.Abs(countConsonants + random.Next(1, 5)).ToString();
                Managers.UI.options[3].text = Math.Abs(countConsonants + random.Next(1, 5)).ToString();
                Managers.UI.options[winOption].text = countConsonants.ToString();
                break;

            case GameMode.CountVowels:
                int countVowels = word.Count(c => vowels.Contains(c));
                Managers.UI.options[0].text = Math.Abs(countVowels + random.Next(-5, -1)).ToString();
                Managers.UI.options[1].text = Math.Abs(countVowels + random.Next(1, 5)).ToString();
                Managers.UI.options[2].text = Math.Abs(countVowels + random.Next(-5, -1)).ToString();
                Managers.UI.options[3].text = Math.Abs(countVowels + random.Next(1, 5)).ToString();
                Managers.UI.options[winOption].text = countVowels.ToString();
                break;

            case GameMode.ReverseWord:
                Managers.UI.options[0].text = new string(ScrambleWord(word).Reverse().ToArray());
                Managers.UI.options[1].text = new string(ScrambleWord(word).Reverse().ToArray());
                Managers.UI.options[2].text = new string(ScrambleWord(word).Reverse().ToArray());
                Managers.UI.options[3].text = new string(ScrambleWord(word).Reverse().ToArray());
                Managers.UI.options[winOption].text = new string(word.Reverse().ToArray());
                break;

            case GameMode.Maths:
                double result = Math.Round(Evaluate(word),3);
                Managers.UI.options[0].text = (result + random.Next(-10, -1)).ToString();
                Managers.UI.options[1].text = (result + random.Next(1, 10)).ToString();
                Managers.UI.options[2].text = (result + random.Next(1, 10)).ToString();
                Managers.UI.options[3].text = (result + random.Next(-10, -1)).ToString();
                Managers.UI.options[winOption].text = result.ToString();
                break;
        }

    }

    public static double Evaluate(string expression)
    {
        var doc = new System.Xml.XPath.XPathDocument(new System.IO.StringReader("<r/>"));
        var nav = doc.CreateNavigator();
        var newString = expression.Replace("x", "*");
        newString = (new System.Text.RegularExpressions.Regex(@"([\+\-\*])")).Replace(newString, " ${1} ");
        newString = newString.Replace("÷", " div ");
        return (double)nav.Evaluate("number(" + newString + ")");
    }



    string ScrambleWord(string wordToScramble)
    {
        StringBuilder jumbleSB = new StringBuilder();
        jumbleSB.Append(wordToScramble);
        int lengthSB = jumbleSB.Length;
        for (int i = 0; i < lengthSB; ++i)
        {
            int index1 = (random.Next() % lengthSB);
            int index2 = (random.Next() % lengthSB);

            Char temp = jumbleSB[index1];
            jumbleSB[index1] = jumbleSB[index2];
            jumbleSB[index2] = temp;
        }
        return jumbleSB.ToString();
    }


    public void LoadPLayerStats()
    {
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            JsonUtility.FromJsonOverwrite(dataAsJson, stats);
        }
    }

    public void SavePLayerStats()
    {
        string dataAsJson = JsonUtility.ToJson(stats);
        File.WriteAllText(filePath, dataAsJson);
    }

}