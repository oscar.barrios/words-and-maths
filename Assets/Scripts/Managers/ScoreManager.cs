﻿using UnityEngine;
using System.Collections;
using System;

public class ScoreManager : MonoBehaviour {
    
    static int oneStarPoints = 50;
    static int twoStarPoints = 100;
    static int threeStarPoints = 150;

    public int[] scoresPerLevel = {
        oneStarPoints + 10, 
        twoStarPoints + 20, 
        threeStarPoints + 30, 
        threeStarPoints + 50, 
        threeStarPoints + 75, 
        threeStarPoints + 100
    };

	public int currentScore;
	public int highScore;
    public int extraLifes;
    public int stars;

    void Awake()
    {
        if (Managers.Game.stats.highScore != 0)
        {
            highScore = Managers.Game.stats.highScore;
            Managers.UI.inGameUI.UpdateScoreUI();
        }
        else
        {
            highScore = 0;
            Managers.UI.inGameUI.UpdateScoreUI();
        }
    }

    public void OnScore(ShapeType type)
	{	
        currentScore += (int)type + 3;
        CheckHighScore();
        SetupSpawner();
        Managers.Game.stats.totalScore += (int)type + 3;

        if ((stars == 0 && currentScore > oneStarPoints) || 
            (stars == 1 && currentScore > twoStarPoints) || 
            (stars == 2 && currentScore > threeStarPoints)) 
        {
            stars++;
            extraLifes += 2 ;
            Managers.UI.PlayAnimationStars(stars);
        }

        if (currentScore > scoresPerLevel[Math.Min(Managers.Game.stats.level-1, scoresPerLevel.Length - 1)]){
            Managers.Score.OnLevelCompleted();
        }

        Managers.UI.inGameUI.UpdateScoreUI();

    }

    private void SetupSpawner()
    {
        if (Managers.Game.stats.level > 4){
            Managers.Spawner.maxLength = 10;
            Managers.Spawner.firstOperator = 0;
            Managers.Spawner.lastOperator = 4;
            Managers.Spawner.maxNumberSize = 30;
            return;
        }

        if (currentScore > threeStarPoints)
        {
            Managers.Spawner.maxLength = 8;
            Managers.Spawner.firstOperator = 2;
            Managers.Spawner.lastOperator = 4;
            Managers.Spawner.maxNumberSize = 20;
        }
        else if (currentScore > twoStarPoints)
        {
            Managers.Spawner.maxLength = 7;
            Managers.Spawner.firstOperator = 0;
            Managers.Spawner.lastOperator = 3;
            Managers.Spawner.maxNumberSize = 20;
        }
        else if (currentScore > oneStarPoints)
        {
            Managers.Spawner.maxLength = 6;
            Managers.Spawner.firstOperator = 0;
            Managers.Spawner.lastOperator = 3;
            Managers.Spawner.maxNumberSize = 10;
        }
        else
        {
            Managers.Spawner.firstOperator = 0;
            Managers.Spawner.lastOperator = 2;
            Managers.Spawner.maxNumberSize = 10;
        }
    }

    public void CheckHighScore()
    {
        if (highScore < currentScore)
        {
            highScore = currentScore;
        }
    }

    public void ResetScore()
    {
        currentScore = 0;
        extraLifes = 0;
        highScore = Managers.Game.stats.highScore;
        stars = 0;
        Managers.UI.inGameUI.UpdateScoreUI();
    }

    internal void OnLostWord()
    {
        extraLifes--;
        Managers.UI.inGameUI.UpdateScoreUI();

        if (extraLifes < 1)
        {
            Managers.Game.SetState(typeof(GameOverState));
        }
    }

    internal void OnLevelCompleted(){
        Managers.Game.SetState(typeof(WinLevelState));
    }
}
