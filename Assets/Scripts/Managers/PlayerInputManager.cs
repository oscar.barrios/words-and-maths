﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerInputManager : MonoBehaviour
{
    public string userChoice;

    public void OptionPress(int userChoice)
    {

        if (Managers.UI.options[userChoice].text.Equals(Managers.UI.options[Managers.Game.winOption].text))
        {
            Managers.Game.currentShape.movementController.Win();
        }
        else
        {
            Managers.Game.currentShape.movementController.Lose();
        }
    }

// SHOW SOFT BUTTONS IN ANDROID AND IMPLEMENT BACK BUTTON
#if UNITY_ANDROID
    void Start()
    {
        Screen.fullScreen = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
#endif

}
