﻿using UnityEngine;
using UnityEngine.UI;
using System;
#if UNITY_ADS
using UnityEngine.Advertisements;
#endif
using System.Collections;

public class AdvertisementManager : MonoBehaviour
{
    public void ShowDefaultAd()
    {
#if UNITY_ADS
        Debug.Log(string.Format("Platform is {0}supported\nUnity Ads {1}initialized", Advertisement.isSupported ? "" : "not ", Advertisement.isInitialized ? "" : "not "));
        
        if (!Advertisement.IsReady())
        {
            Debug.Log("Ads not ready for default zone");
            return;
        }

        Advertisement.Show();
#endif
    }

    public void ShowRewardedAd()
    {
        const string RewardedZoneId = "rewardedVideo";
#if UNITY_ADS
        if (!Advertisement.IsReady(RewardedZoneId))
        {
            Debug.Log(string.Format("Ads not ready for zone '{0}'", RewardedZoneId));
            return;
        }

        var options = new ShowOptions { resultCallback = HandleShowResult };
        Advertisement.Show(RewardedZoneId, options);
#endif
    }

    internal bool isShowing()
    {
        return Advertisement.isShowing;
    }

#if UNITY_ADS
    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                Managers.Game.temporalSpeedBonus = 0.5f;
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }

        Managers.UI.popUps.bonusAdsPopUp.SetActive(false);
        Managers.UI.ActivateUI(Menus.INGAME);
        Managers.Game.SetState(typeof(GamePlayState));
    }
#endif
}
