﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public enum Menus
{
	MAIN,
	INGAME,
	GAMEOVER
}

public class UIManager : MonoBehaviour {

    public GameObject title;
	public MainMenu mainMenu;
	public InGameUI inGameUI;
    public PopUp popUps;
    public GameObject activePopUp;
    public GameObject panel;
    public Text[] options;

    public Animator flowersAnimator;
    public Animator toolTipsAnimator;
    public Animator inGameAnimator;


    public void Start()
    {
        Managers.UI.ShowPlayToolTip();
    }

    public void ActivateUI(Menus menutype)
	{
		if (menutype.Equals (Menus.MAIN))
		{
            StartCoroutine(ActivateMainMenu());          
		}
		else if(menutype.Equals(Menus.INGAME))
		{
            StartCoroutine(ActivateInGameUI());
		}	
	}

    public void OnWordCompleted(){
        StartCoroutine(OnWordFinished(true));
    }

    public void OnWordLost()
    {
        StartCoroutine(OnWordFinished(false));
    }

    IEnumerator OnWordFinished(bool completed){
        if (completed){
            Managers.Grid.GreenFlash();
            yield return new WaitForSeconds(0.2f);
            Managers.Grid.DisableFlash();
        }
        else
        {
            Managers.Grid.RedFlash();
            yield return new WaitForSeconds(0.2f);
            Managers.Grid.DisableFlash();
        }

    }

    internal void PlayAnimationStars(int stars)
    {
        Managers.Audio.PlayLevelUpSound();

        switch (stars)
        {
            case 1:
                inGameUI.congratsLabel.text = Managers.Localization.Localize("Stars.CongratsOne");
                flowersAnimator.Play("OneStar");
                break;
            case 2:
                inGameUI.congratsLabel.text = Managers.Localization.Localize("Stars.CongratsTwo");
                flowersAnimator.Play("TwoStars");
                break;
            case 3:
                inGameUI.congratsLabel.text = Managers.Localization.Localize("Stars.CongratsThree");
                flowersAnimator.Play("ThreeStars");
                break;
        }
    }


    internal void ShowPlayToolTip()
    {
        if (Managers.Game.stats.level < 2){
            mainMenu.toottipPlayText.text = Managers.Localization.Localize("Tooltip.Play");
            toolTipsAnimator.Play("ToolTipPlay");
        }
    }

    internal void ShowStatsToolTips()
    {
        if (Managers.Game.stats.level < 2)
        {
            inGameUI.tooltipScoreText.text = Managers.Localization.Localize("Stats.Score");
            inGameUI.tooltipExtraLifesText.text = Managers.Localization.Localize("Stats.ExtraLives");
            inGameUI.tooltipHighScoreText.text = Managers.Localization.Localize("Stats.HighScore");
            inGameAnimator.Play("ToolTipStats");
        }
    }


    IEnumerator ActivateMainMenu()
    {
        inGameUI.InGameUIEndAnimation();
        inGameUI.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        mainMenu.gameObject.SetActive(true);
        mainMenu.MainMenuStartAnimation();
        Managers.Cam.ZoomOut();
        Managers.UI.mainMenu.MainMenuStartAnimation();
        Managers.UI.MainMenuArrange();
        Managers.Grid.DrawLevels();
    }

    IEnumerator ActivateInGameUI()
    {
        mainMenu.MainMenuEndAnimation();       
        yield return new WaitForSeconds(0.3f);
        mainMenu.gameObject.SetActive(false);
        inGameUI.gameObject.SetActive(true);
        inGameUI.InGameUIStartAnimation();
        Managers.UI.ShowStatsToolTips();
    }

    void Update()
    {
        if (activePopUp != null && Managers.Game.State.GetType() != typeof(GameOverState))
            HideIfClickedOutside(activePopUp);
    }

    public void MainMenuArrange()
    {
        if (Managers.Game.isGameActive)
        {
            mainMenu.layout.spacing = 20;
            mainMenu.layout.padding.left = mainMenu.layout.padding.right = 200;
            mainMenu.restartButton.SetActive(true);
        }
        else
        {
            mainMenu.layout.spacing = 50;
            mainMenu.layout.padding.left = mainMenu.layout.padding.right = 250;
            mainMenu.restartButton.SetActive(false);
        }
    }

    private void HideIfClickedOutside(GameObject outsidePanel)
    {
        if (Managers.UI.activePopUp == Managers.UI.popUps.bonusAdsPopUp)
        {
            Managers.Anal.SendBonusAdsSkipAnalytic();
        }

        if (Input.GetMouseButton(0) && outsidePanel.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                outsidePanel.GetComponent<RectTransform>(),
                Input.mousePosition,
                Camera.main))
        {
            outsidePanel.SetActive(false);
            outsidePanel.transform.parent.gameObject.SetActive(false);
            Managers.UI.panel.SetActive(false);
            activePopUp = null;
        }

    }

}
