﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class SpawnManager : MonoBehaviour {

	public GameObject[] shapeTypes;

    static int minChars = 3;

    static string[] operators = {"+", "-", "x", "÷"};

    public int minLength = 3;
    public int maxLength = 6;
    public int maxNumberSize = 10;
    public int firstOperator = 0;
    public int lastOperator = 4;
    
    //Generate a new shape in the board
    public void Spawn()
	{

        System.Random random = new System.Random();

		// Random Game Mode and Word
        GameMode gameMode = (GameMode) random.Next(0, 5);

        string word = "";
        if (gameMode == GameMode.Maths){
            var operation = operators[random.Next(firstOperator, lastOperator)];
            var number2 = random.Next(0, maxNumberSize);
            int number1 = 0;

            if (operation.Equals("÷")){
                var answer = random.Next(0, 10);
                number1 = number2 * answer;
            }
            else
            {
                number1 = random.Next(0, maxNumberSize);
            }

            word = number1.ToString() + operation + number2.ToString();
        }else{
            do
            {
                word = Managers.Localization.Words[random.Next(0, Managers.Localization.Words.Length)];
            } while (word.Length < minLength || word.Length > maxLength);
        }

        int shapeType = word.Length - minChars;

		// Spawn Group at current Position
        GameObject temp = Instantiate(shapeTypes[shapeType]) ;
        Managers.Game.currentShape = temp.GetComponent<WordShape>();
        temp.transform.parent = Managers.Game.blockHolder;

        Managers.Game.currentShape.type = (ShapeType)shapeType;
        Managers.Game.SetUp(word, gameMode);

    }
}
