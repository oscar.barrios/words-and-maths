﻿using UnityEngine;
using System.Collections;

public class GameOverState : _StatesBase {

	#region implemented abstract members of _StatesBase
	public override void OnActivate ()
	{
        StartCoroutine(Managers.Cam.shaker.ShakeAndRun(Activate));
        Debug.Log ("<color=green>Game Over1 State</color> OnActive");	
	}

	public override void OnDeactivate ()
    {
        //Managers.Adv.ShowDefaultAd();
        Debug.Log ("<color=red>Game Over1 State</color> OnDeactivate");
	}

	public override void OnUpdate ()
	{
		//Debug.Log ("<color=yellow>Game Over State</color> OnUpdate");
	}

    internal IEnumerator Activate(){
        Managers.Game.isGameActive = false;
        Managers.Game.stats.highScore = Managers.Score.highScore;
        Managers.Game.stats.numberOfGames++;
        Managers.Game.SavePLayerStats();
        Managers.UI.popUps.ActivateGameOverPopUp();
        Managers.Audio.PlayLoseSound();
        Managers.Anal.SendScoreAnalytic();
        Managers.Score.ResetScore();
        yield return new WaitForEndOfFrame();
    }

	#endregion

}
