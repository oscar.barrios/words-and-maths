﻿using UnityEngine;
using System.Collections;

public class WinLevelState : _StatesBase {

	#region implemented abstract members of _StatesBase
	public override void OnActivate ()
	{
        Managers.Game.isGameActive = false;
        Managers.Game.stats.highScore = Managers.Score.highScore;
        Managers.Game.stats.numberOfGames++;
        Managers.Game.stats.level++;
        Managers.Game.SavePLayerStats();
        Managers.Audio.PlayWinSound();
        Managers.UI.popUps.ActivateGameWinLevelPopup();
        Managers.Anal.SendScoreAnalytic();
        Managers.Score.ResetScore();
        Debug.Log ("<color=green>Game Win Level State</color> OnActive");	
	}

	public override void OnDeactivate ()
    {
        Managers.Adv.ShowDefaultAd();
        Debug.Log ("<color=red>Game Win Level State</color> OnDeactivate");
	}

	public override void OnUpdate ()
	{
        //Debug.Log ("<color=yellow>Game Win Level State</color> OnUpdate");
	}

	#endregion

}
